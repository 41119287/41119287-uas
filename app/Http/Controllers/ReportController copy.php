<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\ModelPengiriman;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class ReportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $data = ModelPengiriman::all();
        return view('report',compact('data'));
    }

    public function getReportDonut()
    {
        return view('donutChart');
    }

}

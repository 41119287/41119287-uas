-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 22, 2023 at 12:24 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.4.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sales`
--

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `id` int(11) NOT NULL,
  `kode_barang` varchar(50) NOT NULL,
  `nama_barang` varchar(200) DEFAULT NULL,
  `deskripsi` text,
  `stok_barang` int(11) DEFAULT NULL,
  `harga_barang` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`id`, `kode_barang`, `nama_barang`, `deskripsi`, `stok_barang`, `harga_barang`, `updated_at`, `created_at`) VALUES
(2, '1687516796', 'sepatu', 'biru', 202, 500000, '2023-06-23 03:40:48', '2023-06-23 03:39:56'),
(6, '1687519935', 'baju', 'baju baru', 150, 25000, '2023-06-23 04:32:15', '2023-06-23 04:32:15'),
(7, '1687577305', 'sepatu', 'hitam', 22, 500000, '2023-06-23 20:28:25', '2023-06-23 20:28:25'),
(10, '1688787157', 'celana panjang', 'celana jeans', 78, 25000, '2023-07-22 08:28:08', '2023-07-07 20:32:37'),
(11, '1688787236', 'celana pendek', 'celana kotak kotak', 78, 50000, '2023-07-22 08:36:30', '2023-07-07 20:33:56');

-- --------------------------------------------------------

--
-- Table structure for table `kontak`
--

CREATE TABLE `kontak` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kontak`
--

INSERT INTO `kontak` (`id`, `nama`, `email`, `password`, `alamat`, `created_at`, `updated_at`) VALUES
(2, 'firmansyah', 'it.frman@gmail.com', '123', 's', '2023-05-19 11:36:09', '2023-05-19 11:36:09');

-- --------------------------------------------------------

--
-- Table structure for table `lokasi`
--

CREATE TABLE `lokasi` (
  `id` int(11) NOT NULL,
  `kode_lokasi` varchar(10) DEFAULT NULL,
  `nama_lokasi` varchar(255) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lokasi`
--

INSERT INTO `lokasi` (`id`, `kode_lokasi`, `nama_lokasi`, `updated_at`, `created_at`) VALUES
(2, '1', 'jakarta timur', '2023-07-22 08:27:44', '2023-06-23 03:42:38'),
(3, '2', 'Jawa Tengah', '2023-07-22 08:27:48', '2023-06-23 20:28:55');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2023_06_02_125200_add_api_token_field_users', 1),
(5, '2023_06_23_101530_add_alamat_to_users_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pengiriman`
--

CREATE TABLE `pengiriman` (
  `id` int(11) NOT NULL,
  `no_pengiriman` varchar(15) NOT NULL,
  `tanggal` date NOT NULL,
  `lokasi_id` int(11) DEFAULT NULL,
  `barang_id` int(11) DEFAULT NULL,
  `jumlah_barang` int(11) NOT NULL,
  `harga_barang` int(11) DEFAULT NULL,
  `kurir_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengiriman`
--

INSERT INTO `pengiriman` (`id`, `no_pengiriman`, `tanggal`, `lokasi_id`, `barang_id`, `jumlah_barang`, `harga_barang`, `kurir_id`) VALUES
(1, '1', '2023-07-11', 1, 1, 20, 15000, 1),
(2, 'tes1', '2023-07-18', 3, 2, 70, 105000, 1),
(3, 'tes2', '2023-07-18', 1, 1, 50, 100000, 1),
(4, 'tes3', '2023-07-19', 3, 2, 30, 45000, 1),
(5, 'tes4', '2023-07-19', 3, 1, 40, 80000, 1),
(6, 'tes5', '2023-07-20', 2, 3, 60, 60000, 1),
(7, '1234', '2023-07-22', 1, 10, 22, 550000, NULL),
(8, '12344421312', '2023-07-22', 2, 11, 22, 1100000, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `api_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alamat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `api_token`, `alamat`) VALUES
(1, 'firmansyah', 'it.frman@gmail.com', '$2y$10$6g2/.nZzv.2BgzpvyfVbmuiHmjDBYMqmn4UKUtoTx3h7yfjqr9ZJ6', 'mV4CAtvA2Rncya9ONvE3HZVVIXztfVW5moqpPSyd3uSDIeMc6Pt2M3U2hJu8', '2023-06-23 02:32:25', '2023-06-23 03:35:11', 'vjZhVCqCxBOPoKFplkLX1fJjti38uOU1cQxcLdZriSQ78jtIYBIimbT7pXk5', 'Jakarta'),
(3, 'hana', 'tkj.frman@gmail.com', '$2y$10$0K8kaCa.LmKhS42FomL.Nevf.SXYaa2pcKsasdHss3R1NgUU0nh6O', 'BpBZ9UjORdVmcwpMLZpiW1NoFVln0vjiDozNoepThuEQTPEWxo4dmv2dyZyQ', '2023-06-23 04:02:18', '2023-06-23 04:02:18', 'ACnUa5cStxjJwsT1zPDs1bf9Obx30IYuGkMrQCHuLDodLd5pUfmlk6zRNRZC', 'jakarta'),
(4, 'tes', 'asalbuat@123.com', '$2y$10$ZjXwWX.pmadgT0CiJAaCN.2cnDUGKRIEoncGhZ9FtjhWwLFflg3PS', 'H37eFVf8fLInSYSq0JuB3RTljKlJkpzqwjJHnngKFQwouf3lsQQx1nXwpOlz', '2023-06-23 04:04:56', '2023-06-23 04:04:56', '0KyEV6hYaLO6wvEa6Ees6YB6DhPaRxohkrBbH8QsN5r242nZWeZfsYIObtgU', 'bekasi'),
(5, 'firmansyah', 'firman@gmail.com', '$2y$10$76r8H2iNR4gWJjeOOiMdVeavqiHHrLmp/nz9yRB3JwYKeonMOf1Ku', 'bZNOY5dGV8Shc3QkZko3XlrIjxTn3tjlYRoXC0UWlolnHxkAdN6VKN3abcbs', '2023-07-07 20:31:06', '2023-07-07 20:31:06', 'SSbI5HKGCCjhrOvwq33RGnBOguHXHYISo11tU9ZZV169I25WNYYpUEE9U6sG', 'jakarta');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lokasi`
--
ALTER TABLE `lokasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `pengiriman`
--
ALTER TABLE `pengiriman`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_api_token_unique` (`api_token`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `barang`
--
ALTER TABLE `barang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `lokasi`
--
ALTER TABLE `lokasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `pengiriman`
--
ALTER TABLE `pengiriman`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

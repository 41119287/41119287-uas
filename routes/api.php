<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::middleware('auth:api')->get('/barang', 'api\DataBarangController@index');
Route::middleware('auth:api')->post('/barang', 'api\DataBarangController@store');
Route::middleware('auth:api')->get('/outlet', 'api\DataOutletController@index');

// Route::get('datapengiriman', 'API\DataPengirimanController@index');
// Route::resource('barang','API\DataBarangController');
// Route::resource('outlet','API\DataOutletController');
// Route::resource('datapengiriman','API\DataPengirimanController');
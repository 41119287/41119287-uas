<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('kontak','Kontak');
Route::resource('lokasi','LokasiController');
Route::resource('pengiriman','PengirimanController');
Route::resource('barang','BarangController');
Route::resource('report','ReportController');


Route::get('/','ReportController@index')->name('homez');
// Route::get('/pengiriman','PengirimanController@index')->name('datakirim');
Route::get('/donut', 'ReportController@index')->name('donut');
Route::get('/lokasichart', 'ReportController@lokasiChart')->name('lokasiChart');
Route::get('/barangchart', 'ReportController@barangChart')->name('barangChart');

Route::get('/dashboard', function()
{

        return view('dashboard.index', [
            'sub' => "Dashboard Pengiriman"
        ]);
});

Route::get('/input_data', function()
{
        return view('dashboard.input_data', [
            'sub' => "Masukan Data"
        ]);

});
Route::post('/input_data', 'ReportController@inputData')->name('inputData');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

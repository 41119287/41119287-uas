@extends('base')
@section('content')
<div class="row">
  <div class="col-lg-4 col-6">
  
  <div class="small-box bg-info">
  <div class="inner">
  <h3>150</h3>
  <p>Total Pengiriman</p>
  </div>
  <div class="icon">
  <i class="ion ion-bag"></i>
  </div>
  <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
  </div>
  </div>
  
  <div class="col-lg-4 col-6">
  
  <div class="small-box bg-success">
  <div class="inner">
  <h3>Jakarta<sup style="font-size: 20px"></sup></h3>
  <p>Lokasi Terbanyak</p>
  </div>
  <div class="icon">
  <i class="ion ion-stats-bars"></i>
  </div>
  <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
  </div>
  </div>
  
  <div class="col-lg-4 col-6">
  
  <div class="small-box bg-warning">
  <div class="inner">
  <h3>44</h3>
  <p>Barang Terbanyak</p>
  </div>
  <div class="icon">
  <i class="ion ion-person-add"></i>
  </div>
  <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
  </div>
  
  
  
  </div>
    <canvas id="myChart" style="height:40vh; width:30vw; margin:0 auto"></canvas>
  </div>

  <div>
    <canvas id="hargaBarangChart" style="height:40vh; width:30vw; margin:0 auto"></canvas>
  </div>
  
  <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
  
  <script>
    const ctx = document.getElementById('myChart');
  
    new Chart(ctx, {
      type: 'doughnut',
      data: {
        labels: ['Pengiriman Diatas 100', 'Pengiriman DIbawah 101'],
        datasets: [{
          label: 'Pengiriman barang diatas 100',
          data: [12, 19],
          borderWidth: 1
        }]
      },
      options: {
        scales: {
          y: {
            beginAtZero: true
          }
        }
      }
    });
  </script>
  <script>
    const chart = document.getElementById('hargaBarangChart');
  
    new Chart(chart, {
      type: 'doughnut',
      data: {
        labels: ['Harga Diatas 1000', 'Harga DIbawah 1001'],
        datasets: [{
          label: 'Harga barang diatas 1000',
          data: [12, 19],
          borderWidth: 1
        }]
      },
      options: {
        scales: {
          y: {
            beginAtZero: true
          }
        }
      }
    });
  </script>
   
@endsection